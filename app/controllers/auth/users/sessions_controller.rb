class Auth::Users::SessionsController < Devise::SessionsController
  layout 'app'

  def after_sign_in_path_for(resource)
    page_path(resource)
  end

  def after_sign_out_path_for(resource)
    root_path
  end
end
