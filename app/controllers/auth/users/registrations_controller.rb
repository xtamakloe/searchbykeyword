class Auth::Users::RegistrationsController < App::BaseController
  def new
    @user = User.new
  end


  def create
    @user = User.new(user_params)

    error_messages = []

    if @user.email
      if User.where(email: @user.email).first
        error_messages << "Could not create account! Email already taken."
      else
        @user.account = Account.create(title: @user.email.split('@').first)
      end
    end

    if error_messages.empty? && @user.save
      sign_in @user

      flash[:success] = 'Welcome!'

      redirect_to root_path
    else
      account.destroy if account

      if error_messages.empty?
        error_messages << @user.errors.full_messages.first
      end

      flash[:error] = "An error occurred, please try again. <br/><br/> #{error_messages.first}".html_safe
      # "If you still have problems signing up, drop us an "\
      # "<a href='mailto:hello@themenote.com?subject=Sign Up Issues' "\
      # "style='color:#721c24;font-weight:bold'>email</a>".html_safe

      render "new"
    end
  end


  private


  def user_params
    # NOTE: Using `strong_parameters` gem
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
