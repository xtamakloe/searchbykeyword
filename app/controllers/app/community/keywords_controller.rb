class App::Community::KeywordsController < App::BaseController
  def show
    @keywords = Keyword.shared
  end
end
