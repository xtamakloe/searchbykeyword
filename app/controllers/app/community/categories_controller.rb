class App::Community::CategoriesController < App::BaseController
  def show
    @category = Category.find(params[:id])
  end
end
