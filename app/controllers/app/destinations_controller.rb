class App::DestinationsController < App::BaseController

  # def create
  #   terms       = destination_params[:terms]
  #   keyword     = Keyword.search(current_account, terms)
  #   terms_array = terms.split # split terms
  #   url         =
  #     if keyword
  #       terms_array.shift # remove keyword
  #       keyword.generate_destination_url(terms_array)
  #     else
  #       "https://duckduckgo.com/?q=#{terms_array.join('+')}&t=h_&ia=web"
  #     end
  #   redirect_to url
  # end


  def create
    terms = destination_params[:terms]
    res   = Keyword.search_multiple(current_account, terms)
    url   = Keyword.generate_destination_url(res[:keywords], res[:terms])

    redirect_to url
  end


  private


  def destination_params
    params.require(:destination).permit(:terms)
  end
end
