class App::PagesController < App::BaseController

  def show
    @keywords = current_account ? current_account.keywords : Keyword.shared
  end

  def test_page
    render plain: 'ok'
  end
end
