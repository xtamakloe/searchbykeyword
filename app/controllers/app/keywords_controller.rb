class App::KeywordsController < App::BaseController
  before_action :authenticate_user!
  before_action :load_categories


  def show
  end


  def index
    @keywords = @categories.map(&:keywords).flatten
  end


  def new
    @keyword = current_account.keywords.new
    if params[:id]
      @from_keyword         = Keyword.shared.find(params[:id])
      @keyword.title        = @from_keyword.title
      @keyword.word         = @from_keyword.word
      @keyword.url_template = @from_keyword.url_template
      @keyword.category_id  = @from_keyword.category_id
      @keyword.description  = @from_keyword.description
      @keyword.usage        = @from_keyword.usage
      flash.now[:notice]    = "Confirm details to add the keyword to your account"
    end
  end


  def create
    @keyword = current_account.keywords.new(keyword_params)
    if @keyword.save
      flash[:success] = "Keyword added!"

      redirect_to root_path
    else
      error_message     = @keyword.errors.full_messages.first
      flash.now[:error] = "Error adding keyword: <br>"\
                          "#{error_message}".html_safe

      render 'new'
    end
  end


  def edit
    @keyword = current_account.keywords.find(params[:id])

    render 'new'
  end


  def update
    @keyword = current_account.keywords.find(params[:id])
    if @keyword.update_attributes(keyword_params)
      flash[:success] = "Keyword '#{@keyword.word}' updated!"

      redirect_to root_path
    else
      error_message     = @keyword.errors.full_messages.first
      flash.now[:error] = "Error updating keyword: "\
                          "<br/>#{error_message}".html_safe

      render 'new'
    end
  end


  def destroy
    @keyword = current_account.keywords.find(params[:id])
    if @keyword.destroy
      flash[:success] = "Keyword '#{@keyword.word}' removed"

      redirect_to root_path
    else
      flash.now[:error] = 'Error removing keyword'

      render 'new'
    end
  end


  private


  def load_categories
    # this depends on current user i think
    @categories = Category.all
  end


  def keyword_params
    params.require(:keyword).permit(
      :title,
      :word,
      :url_template,
      :category_id,
      :shared,
      :description,
      :usage
    )
  end
end
