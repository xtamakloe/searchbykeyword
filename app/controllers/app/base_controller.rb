class App::BaseController < ApplicationController
  layout 'app'

  helper_method :current_account

  def current_account
    @current_account ||= current_user.try(:account)
  end
end
