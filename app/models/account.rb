class Account < ApplicationRecord

  has_many :users, dependent: :destroy
  has_many :keywords, dependent: :destroy
  has_many :pages, dependent: :destroy

  validates :title, presence: true, uniqueness: true
end
