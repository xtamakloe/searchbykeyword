class Category < ApplicationRecord
  default_scope {order('title ASC')}

  DEFAULT_CATEGORIES = %w(Search Shopping Social Images Videos Books Travel Finance News Research)

  has_many :keywords
end
