class Keyword < ApplicationRecord

  TERMS_PLACEHOLDER_REGEX = /\%s/i

  belongs_to :account
  belongs_to :category

  validates :title, presence: true
  validates :word,
            presence:   true,
            uniqueness: { scope: :account_id, message: "already used" },
            format:     { with: /^[a-zA-Z0-9]*$/, multiline: true }
  validates :url_template, presence: true
  validate :url_template_format, :uniqueness_of_shared_word

  before_save :convert_word_to_downcase


  scope :shared, -> { where(shared: true) }


  # +terms_array+:: +Array+ terms without keyword
  def generate_destination_url(terms_array)
    if terms_array && terms_array.empty?
      uri = URI.parse(URI.encode(self.url_template))
      "#{uri.scheme}://#{uri.host}"
    else
      self.url_template.gsub(TERMS_PLACEHOLDER_REGEX, terms_array.join('+'))
    end
  end


  # +keyword_array+:: +Array+ of +Keyword+ objects
  # +terms_array+:: +Array+ of terms
  def self.generate_destination_url(keywords_array, terms_array)
    # "https://www.google.com/search?q="\
    #   "reits+site%3Amoney.cnn.com+OR+site%3Asmartmoney.com+"\
    #   "OR+site%3Akiplinger.com&oq=reits+site%3Amoney.cnn.com+"\
    #   "OR+site%3Asmartmoney.com+OR+site%3Akiplinger.com"

    search_url = "https://www.google.com/search?q="
    sites      = []

    if keywords_array.size == 1
      keywords_array.first.generate_destination_url(terms_array)

    elsif keywords_array.size > 1
      keywords_array.each do |keyword|
        uri = URI.parse(URI.encode(keyword.url_template))
        sites << "site%3A#{uri.host}"
      end

      sites_url = sites.join('+OR+')

      "#{search_url}"\
      "#{terms_array.join('+')}+#{sites_url}"\
      "&oq="\
      "#{terms_array.join('+')}+#{sites_url}"
    else
      # Default search URL
      "https://duckduckgo.com/?q=#{terms_array.join('+')}&t=h_&ia=web"
    end

  end


  # +terms+:: String:terms containing keyword
  # +account+:: Account
  def self.search(account, terms)
    terms_array = terms.split
    keyword_str = terms_array.shift
    keywords    =
      if account.present?
        account.keywords
      else
        Keyword.shared
      end
    keywords.where(word: keyword_str.downcase).first
  end


  # Searches for +Keyword+s based on string supplied and reeturns
  # a list of +Keyword+s found
  # +terms+::+String+ of terms, which contains one or more keywords
  # +account+:: +Account+ object
  def self.search_multiple(account, terms)
    keywords =
      if account.present?
        account.keywords
      else
        Keyword.shared
      end

    found_keywords, found_terms = [], []

    terms.split.each do |term|
      found_keyword = keywords.where(word: term.downcase).first
      if found_keyword
        found_keywords << found_keyword
      else
        found_terms << term
      end
    end

    { keywords: found_keywords, terms: found_terms }
  end


  private


  def url_template_format
    if url_template.present?
      unless url_template.match(TERMS_PLACEHOLDER_REGEX)
        errors.add(:url_template, "must include placeholder")
      end

      unless url_template.start_with?('https://', 'http://')
        errors.add(:url_template, "must start with an http protocol")
      end
    end
  end


  def convert_word_to_downcase
    self.word.downcase!
  end


  def uniqueness_of_shared_word
    if self.shared && Keyword.shared.where(word: self.word).first
      errors.add(:base,
                 "The keyword chosen has already been assigned. <br/>"\
                 "Keywords should be unique in order to be shared with the community. <br/>"\
                 "Please try again with a different keyword or save without sharing.")
    end
  end
end
