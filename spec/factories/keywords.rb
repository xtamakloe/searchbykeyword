FactoryBot.define do
  factory :keyword do
    title {'YouTube'}
    word {'yt'}
    url_template {'https://www.youtube.com/results?search_query=%s'}
    shared { false }
    description { Faker::Lorem.sentence }
    usage { Faker::Lorem.sentence }

    account
    category
  end
end