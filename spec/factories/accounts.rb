FactoryBot.define do
  factory :account do
    title { Faker::Name.first_name }
  end
end