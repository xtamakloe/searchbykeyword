FactoryBot.define do
  factory :user do
    email {'user@email.com'}
    password {'password'}

    account
  end
end