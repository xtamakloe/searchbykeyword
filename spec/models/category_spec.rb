require 'rails_helper'

RSpec.describe Category, type: :model do
  let(:category) {build(:category)}

  it 'has a valid factory' do
    expect(category).to be_valid

    expect(category.title).to eq('Shopping')
  end

  describe '(Validations):' do
  end


  describe "(Associations):" do
    it 'has many keywords' do
      expect(category).to have_many(:keywords)
    end
  end

  describe "(Methods):" do
    describe ".keywords" do
      it 'should return keywords in alphabetical order'
    end
  end

end