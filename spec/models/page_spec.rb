require 'rails_helper'

RSpec.describe Page, type: :model do
  let(:page) {build(:page)}

  it 'has a valid factory' do
    expect(page).to be_valid
  end

  describe "Associations" do
    it 'belongs to an account' do
      expect(page).to belong_to(:account)
    end
  end
end