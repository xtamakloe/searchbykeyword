require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) {build(:user)}

  it 'has a valid factory' do
    expect(user).to be_valid
  end

  describe "Associations" do
    it 'belongs to an account' do
      expect(user).to belong_to(:account)
    end
  end
end