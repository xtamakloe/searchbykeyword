require 'rails_helper'

RSpec.describe Keyword, type: :model do

  let(:acct) { create(:account) }
  let(:cat) { create(:category) }
  let(:keyword) { create(:keyword, account: acct, category: cat) }

  it 'has a valid factory' do
    expect(keyword.word).to eq('yt')
    expect(keyword.url_template).to eq('https://www.youtube.com/results?search_query=%s')
    expect(keyword).to be_valid
  end

  describe 'Associations' do
    it 'belongs to an account' do
      expect(keyword).to belong_to(:account)
    end

    it 'belongs to a category' do
      expect(keyword).to belong_to(:category)
    end
  end


  describe '(Validations):' do
    it 'is not valid without a title' do
      expect(keyword).to validate_presence_of(:title)
    end

    it 'is not valid without a word' do
      expect(keyword).to validate_presence_of(:word)
    end

    it 'is not valid without a URL template' do
      expect(keyword).to validate_presence_of(:url_template)
    end

    describe "has a valid word:" do
      it 'should not have spaces in provided string' do
        keyword.word = 'space word'

        expect(keyword).to_not be_valid
      end

      it 'should be unique for an account' do
        expect(keyword).to validate_uniqueness_of(:word).scoped_to(:account_id).with_message('already used')
      end

      it 'should save keyword in downcase' do
        keyword_1 = create(:keyword, word: 'YT', account: acct, category: cat)

        expect(keyword_1.word).to eq('yt')
      end

      it "should be unique if shared" do
        create(:keyword, word: 'yt', shared: true, account: acct, category: cat)
        create(:keyword, word: 'fb', account: acct, category: cat)
        create(:keyword, word: 'az', account: acct, category: cat)

        keyword = build(:keyword,
                        word:     'yt',
                        shared:   true,
                        category: cat,
                        account:  create(:account)
        )
        keyword.save

        expect(keyword).to_not be_valid
      end
    end

    describe 'has a valid url template:' do
      it 'should include placeholder' do
        keyword.url_template = 'https://www.youtube.com/results?search_query='

        expect(keyword).to_not be_valid
      end

      it 'should start with a protocol' do
        keyword.url_template = 'www.youtube.com/results?search_query=%s'

        expect(keyword).to_not be_valid
      end
    end

    describe "has a boolean flag to determine if it is a public" do
      it 'should always be set and be false by default'
    end

  end

  describe '(Scopes):' do
    describe ".shared" do
      it 'should return only keywords that have shared flag set to true' do

        k1 = create(:keyword, word: 'az', shared: false, account: create(:account))
        k2 = create(:keyword, word: 'yt', shared: true, account: create(:account))
        k3 = create(:keyword, word: 'fb', shared: false, account: create(:account))

        expect(Keyword.shared).to eq([k2])
      end
    end
  end

  describe '(Methods):' do
    before(:example) do
      @category = create(:category)
      @account  = create(:account)

      @keyword_yt = create(:keyword,
                           word:         'yt',
                           url_template: 'https://www.yt.com/search?q=%s',
                           account:      @account,
                           category:     @category)
      @keyword_fb = create(:keyword,
                           word:         'fb',
                           url_template: 'https://www.fb.com/search?q=%s',
                           account:      @account,
                           category:     @category)
      @keyword_az = create(:keyword,
                           word:         'az',
                           url_template: 'https://www.az.com/search?q=%s',
                           account:      @account,
                           category:     @category)

      @terms    = 'yt az lebron james fb'
      @keywords = [@keyword_yt, @keyword_az, @keyword_fb]
    end

    describe ".search" do
      before(:example) do
        @category      = create(:category)
        @user1_account = create(:account, title: 'account-1')
        @user2_account = create(:account, title: 'account-2')
        @user3_account = create(:account, title: 'account-3')

        @user1_keyword  = create(:keyword,
                                 account:  @user1_account,
                                 category: @category)
        @user2_keyword  = create(:keyword,
                                 account:  @user2_account,
                                 category: @category)
        @user3_keyword  = create(:keyword,
                                 shared:   true,
                                 account:  @user3_account,
                                 category: @category)
        @terms          = 'yt lebron james'
      end

      context 'when no user is logged in (no account present)' do
        it "searches for keyword in shared keywords" do
          expect(Keyword.search(nil, @terms)).to eq(@user3_keyword)
        end
      end

      context 'when user is logged in (account present)' do
        it "searches for keyword in user's account" do
          expect(Keyword.search(@user1_account, @terms)).to eq(@user1_keyword)
        end
      end
    end


    describe ".search_multiple" do
      it "returns an array of Keyword objects for each keyword specified" do
        expect(Keyword.search_multiple(@account, @terms)[:keywords]).to eq([@keyword_yt, @keyword_az, @keyword_fb])
      end

      it "returns an array of search terms that are not keywords" do
        expect(Keyword.search_multiple(@account, @terms)[:terms]).to eq(['lebron', 'james'])
      end
    end


    describe '.generate_destination_url(keywords_array, terms_array):' do
      context 'when one keyword is provided' do
        it 'generates a valid destination url if only search terms are provided' do
          expected_url = 'https://www.yt.com/search?q=lebron+james'
          output_url   = Keyword.generate_destination_url([@keyword_yt], ['lebron', 'james'])

          expect(output_url).to eq(expected_url)
        end

        it 'returns root domain if search terms are not provided' do
          expected_url = 'https://www.yt.com'
          output_url   = Keyword.generate_destination_url([@keyword_yt], [])

          expect(output_url).to eq(expected_url)
        end
      end

      context 'when multiple keywords are provided' do
        it 'should generate a google multi-site search using root domains of keywords' do
          expected_url = "https://www.google.com/search?q="\
                        "lebron+james+"\
                        "site%3Awww.yt.com+OR+site%3Awww.az.com+OR+site%3Awww.fb.com"\
                        "&oq=lebron+james+"\
                        "site%3Awww.yt.com+OR+site%3Awww.az.com+OR+site%3Awww.fb.com"
          output_url   = Keyword.generate_destination_url([@keyword_yt, @keyword_az, @keyword_fb],
                                                          ['lebron', 'james'])

          expect(output_url).to eq(expected_url)
        end
      end

      context "when no keywords are provided" do
        it 'should return nil when no search terms are provided' do
          expected_url = "https://duckduckgo.com/?q=hello+world&t=h_&ia=web"
          output_url   = Keyword.generate_destination_url([], ['hello', 'world'])

          expect(output_url).to eq(expected_url)
        end
      end
    end


    describe '#generate_destination_url(terms_array):' do
      it 'generates a valid destination url if search terms are provided' do
        expected_url = 'https://www.yt.com/search?q=hello+world'
        output_url   = @keyword_yt.generate_destination_url(['hello', 'world'])

        expect(output_url).to eq(expected_url)
      end


      it 'returns root domain if search terms are not provided' do
        expected_url = 'https://www.yt.com'
        output_url   = @keyword_yt.generate_destination_url([])

        expect(output_url).to eq(expected_url)
      end
    end
  end

end