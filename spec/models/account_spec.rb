require 'rails_helper'

RSpec.describe Account, type: :model do

  let(:account) { build(:account, title: 'test') }

  it 'has a valid factory' do
    expect(account.title).to eq('test')

    expect(account).to be_valid
  end

  describe "(Associations)" do
    it 'has many users' do
      expect(account).to have_many(:users).dependent(:destroy)
    end

    it 'has many keywords' do
      expect(account).to have_many(:keywords).dependent(:destroy)
    end

    it 'has many pages' do
      expect(account).to have_many(:pages).dependent(:destroy)
    end
  end

  describe "(Validations)" do
    it 'should always have a title' do
      expect(account).to validate_presence_of(:title)
    end

    it 'should have a unique title' do
      expect(account).to validate_uniqueness_of(:title)
    end
  end

  describe "(Methods)" do
  end

end