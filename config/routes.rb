Rails.application.routes.draw do

  namespace :auth do
    namespace :users do
      get 'registrations/new'
    end
  end
  devise_for :users, controllers: {
    sessions:      'auth/users/sessions',
    registrations: 'auth/users/registrations'
  }

  as :user do
    get '/login', to: 'auth/users/sessions#new'
  end

  scope module: 'app' do
    resources :pages, only: [:show]
    resources :destinations, only: [:create]
    resources :keywords

    namespace :community do
      resource :keywords, only: [:show]
      resources :categories, only: [:show]
    end
  end

  get '/test', to: 'app/pages#test_page'

  root to: 'app/pages#show'
end

