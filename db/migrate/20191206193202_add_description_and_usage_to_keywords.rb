class AddDescriptionAndUsageToKeywords < ActiveRecord::Migration[6.0]
  def change
    add_column :keywords, :description, :text
    add_column :keywords, :usage, :string
  end
end
