class AddUrlTemplateToKeywords < ActiveRecord::Migration[6.0]
  def change
    add_column :keywords, :url_template, :string
  end
end
