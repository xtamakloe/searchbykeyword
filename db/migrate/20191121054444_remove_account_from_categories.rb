class RemoveAccountFromCategories < ActiveRecord::Migration[6.0]
  def change
    change_table :categories do |t|
      t.remove_references :account
    end
  end
end
