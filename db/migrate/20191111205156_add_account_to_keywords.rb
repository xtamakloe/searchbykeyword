class AddAccountToKeywords < ActiveRecord::Migration[6.0]
  def change
    add_reference :keywords, :account, null: false, foreign_key: true
  end
end
