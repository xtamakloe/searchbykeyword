class AddSystemToAccounts < ActiveRecord::Migration[6.0]
  def change
    add_column :accounts, :system, :boolean
  end
end
