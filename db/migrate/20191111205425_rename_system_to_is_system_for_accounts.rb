class RenameSystemToIsSystemForAccounts < ActiveRecord::Migration[6.0]
  def change
    rename_column :accounts, :system, :is_system
  end
end
