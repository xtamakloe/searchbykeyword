class AddTitleToKeywords < ActiveRecord::Migration[6.0]
  def change
    add_column :keywords, :title, :string
  end
end
