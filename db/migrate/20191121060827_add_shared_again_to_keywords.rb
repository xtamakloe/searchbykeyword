class AddSharedAgainToKeywords < ActiveRecord::Migration[6.0]
  def change
    add_column :keywords, :shared, :boolean, default: false
  end
end
