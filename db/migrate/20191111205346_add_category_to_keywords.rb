class AddCategoryToKeywords < ActiveRecord::Migration[6.0]
  def change
    add_reference :keywords, :category, foreign_key: true
  end
end
