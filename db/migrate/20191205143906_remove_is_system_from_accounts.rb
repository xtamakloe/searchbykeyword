class RemoveIsSystemFromAccounts < ActiveRecord::Migration[6.0]
  def change
    remove_column :accounts, :is_system
  end
end
