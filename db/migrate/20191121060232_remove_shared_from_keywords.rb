class RemoveSharedFromKeywords < ActiveRecord::Migration[6.0]
  def change
    remove_column :keywords, :shared
  end
end
