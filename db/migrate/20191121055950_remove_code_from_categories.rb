class RemoveCodeFromCategories < ActiveRecord::Migration[6.0]
  def change
    remove_column :categories, :code
  end
end
